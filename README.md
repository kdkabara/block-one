### Account API
REST service that provides access to bank accounts; Implemented using Spring Boot with Spring Security to secure the endpoints with BASIC auth.

#### Steps to run application
1. `gradlew build`
2. `gradlew bootRun`
3. Access API on localhost:8080, or access [Swagger Page](http://localhost:8080/swagger-ui.html) (auto-generated with springfox) to see available endpoints (serves as API documentation).

Note: The API is secured with BASIC auth - you will need to enter username: **user** and password: **pwd** to be authorized. 

#### Steps to run tests
1. `gradlew build`
2. `gradlew test`

Tests include:
1. Integration tests to test controller (not entirely integration, Spring Boot spins up a container that doesnt start up a server, but tests the web layer below it by handling incoming HTTP requests and passes it to the controller - provides almost entire stack testing without the cost of starting the server).
2. Unit tests to test Service & Repository layers.

#### Techniques/Patterns used

1. All classes use concept of Dependency Injection for managing dependencies. 
2. Strategy pattern used for Credit Facility, since each credit facility will likely have it's own requirements that will be quite distinct.
3. Separation of concerns between credit facility provider and account service provider. Credit facility provider is separated since adding a credit facility will eventually involve more steps.
4. TDD used for development.
5. Testing with JUnit, Mocking with Mockito.

##### Assumptions
1. Single type of account, different account types like joint accounts etc are not allowed.
2. No withdrawal limits; no overdrafts allowed.
3. All users of the API have the authorization to modify/delete accounts.
4. Credit Facilities are simply added - there is no approval process or other checks performed. 

##### Possible Extensions
1. Use BigDecimal instead of double for calculations - this would be the most important in order to avoid miscalculations cause by precision. 
2. Create a repository for CreditFacilities - this would be the more long term solution.
3. Further actions on credit facilities - update, delete;
4. Add validation rules to the endpoints - there is not validation around the end points. e.g. the update endpoint doesnt allow updating the balance/currency/credit facilities of the account but this is jot controlled through validation, rather through some code which can be improved.

[Swagger Page]: http://localhost:8080/swagger-ui.html
