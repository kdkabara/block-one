package com.blockone.entity;

import com.blockone.entity.creditfacility.SimpleLoan;
import com.blockone.error.creditfacility.DoesNotMeetMinimumBalanceException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SimpleLoanTest {
    SimpleLoan simpleLoan = new SimpleLoan(1000, 0.07);
    User user = new User("TestUser");

    @Test
    void loanAddedToAccountIfBalanceIsGreaterThanMinimumBalanceReq() {
        Account account = new Account(user, 100);

        assertThrows(DoesNotMeetMinimumBalanceException.class, () -> simpleLoan.addToAccount(account));
    }

    @Test
    void loanIsNotAddedToAccountIfBalanceIsLessThanMinimumBalanceReq() throws DoesNotMeetMinimumBalanceException {
        Account account = new Account(user, 1200);
        simpleLoan.addToAccount(account);

        assertEquals(account.getCreditFacilities().get(simpleLoan.getId()), simpleLoan);
    }
}
