package com.blockone.controller;

import com.blockone.entity.Account;
import com.blockone.entity.User;
import com.blockone.repository.AccountRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.RequestPostProcessor;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class AccountControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AccountRepository accountRepository;

    private RequestPostProcessor auth = httpBasic("user", "pwd");

    @BeforeEach
    void setUp() {
        accountRepository.insertAccount(new Account(1L, new User("TestUser1"), 1000));
        accountRepository.insertAccount(new Account(2L, new User("TestUser2"), 200));
    }

    /* GET ACCOUNT TESTS */

    @Test
    void getAccountWithoutAuthReturns401() throws Exception {
        this.mockMvc.perform(get("/api/account/1"))
                .andDo(print())
                .andExpect(status().is(401));
    }

    @Test
    void getAccountWithExistingIDReturnsAccountWith200() throws Exception {
        this.mockMvc.perform(get("/api/account/1").with(auth))
                .andDo(print())
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.owner.name").value("TestUser1"));
    }

    @Test
    void getAccountWithNotExistingIDReturns404() throws Exception {
        this.mockMvc.perform(get("/api/account/100").with(auth))
                .andDo(print())
                .andExpect(status().is(404));
    }

    @Test
    void getAccountWithNonNumbericIDReturns404() throws Exception {
        this.mockMvc.perform(get("/api/account/test").with(auth))
                .andDo(print())
                .andExpect(status().is(404));
    }

    /* CREATE ACCOUNT TESTS */

    @Test
    void createAccountWithoutAuthReturns401() throws Exception {
        String account = "{\"owner\": { \"name\": \"TestUser\" }}";
        this.mockMvc.perform(post("/api/account").content(account).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(401));
    }

    @Test
    void createAccountReturns200AndNewlyCreatedAccount() throws Exception {
        String account = "{\"currency\": \"USD\", \"owner\": { \"name\": \"TestUser\", \"contactNumber\": \"12345678\" }}";
        this.mockMvc.perform(post("/api/account").with(auth).content(account).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.owner.name").value("TestUser"))
                .andExpect(jsonPath("$.owner.contactNumber").value("12345678"))
                .andExpect(jsonPath("$.balance").value(0));
    }

    /* UPDATE ACCOUNT TESTS */

    @Test
    void updateAccountWithoutAuthReturns401() throws Exception {
        this.mockMvc.perform(put("/api/account"))
                .andDo(print())
                .andExpect(status().is(401));
    }

    @Test
    void updateAccountReturns200AndNewlyUpdatedAccount() throws Exception {
        String account = "{\"id\": 1, \"owner\": { \"name\": \"ChangedUser\", \"contactNumber\": \"87654321\" }}";
        this.mockMvc.perform(put("/api/account/1").with(auth).content(account).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.owner.name").value("ChangedUser"))
                .andExpect(jsonPath("$.owner.contactNumber").value("87654321"));
    }

    @Test
    void updateAccountDoesntAllowChangingBalanceOrCurrency() throws Exception {
        String account = "{\"id\": 1, \"balance\": 10, \"currency\": \"USD\" }}";
        this.mockMvc.perform(put("/api/account/1").with(auth).content(account).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.balance").value(1000))
                .andExpect(jsonPath("$.currency").value("HKD"));
    }

    @Test
    void updateAccountWithDifferentIDReturns400() throws Exception {
        String account = "{\"id\": 1, \"owner\": { \"name\": \"ChangedUser\", \"contactNumber\": \"87654321\" }}";
        this.mockMvc.perform(put("/api/account/2").with(auth).content(account).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(400));
    }

    @Test
    void updateAccountWithNonNumericIDReturns404() throws Exception {
        String account = "{\"owner\": { \"name\": \"ChangedUser\", \"contactNumber\": \"87654321\" }}";
        this.mockMvc.perform(put("/api/account/test").with(auth).content(account).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(404));
    }

    @Test
    void updateAccountNotExistingIDReturns404() throws Exception {
        String account = "{\"id\": 100, \"owner\": { \"name\": \"ChangedUser\", \"contactNumber\": \"87654321\" }}";
        this.mockMvc.perform(put("/api/account/100").with(auth).content(account).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(404));
    }

    /* DELETE ACCOUNT TESTS */

    @Test
    void deleteAccountWithoutAuthReturns401() throws Exception {
        this.mockMvc.perform(delete("/api/account/1"))
                .andDo(print())
                .andExpect(status().is(401));
    }

    @Test
    void deleteAccountWithExistingIDReturns200() throws Exception {
        this.mockMvc.perform(delete("/api/account/1").with(auth))
                .andDo(print())
                .andExpect(status().is(200));
    }

    @Test
    void deleteAccountWithNotExistingIDReturns404() throws Exception {
        this.mockMvc.perform(delete("/api/account/100").with(auth))
                .andDo(print())
                .andExpect(status().is(404));
    }

    @Test
    void deleteAccountWithNonNumericIDReturns404() throws Exception {
        this.mockMvc.perform(delete("/api/account/test").with(auth))
                .andDo(print())
                .andExpect(status().is(404));
    }

    /* WITHDRAW TESTS */

    @Test
    void withdrawWithoutAuthReturns401() throws Exception {
        this.mockMvc.perform(post("/api/account/1/withdraw"))
                .andDo(print())
                .andExpect(status().is(401));
    }

    @Test
    void withdrawWithExistingIDAndAmountMoreThanBalanceReturns400() throws Exception {
        String transactionRequest = "{\"amount\": 10000 }";
        this.mockMvc.perform(post("/api/account/1/withdraw").with(auth).content(transactionRequest).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(400));
    }

    @Test
    void withdrawWithExistingIDAndInvalidAmountReturns400() throws Exception {
        String transactionRequest = "{\"amount\": -100 }";
        this.mockMvc.perform(post("/api/account/1/withdraw").with(auth).content(transactionRequest).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(400));
    }

    @Test
    void withdrawWithExistingIDAndValidAmountReturnsNewBalanceAnd200() throws Exception {
        String transactionRequest = "{\"amount\": 100 }";
        this.mockMvc.perform(post("/api/account/1/withdraw").with(auth).content(transactionRequest).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.balance").value(900));
    }

    @Test
    void withdrawWithNotExistingIDReturns404() throws Exception {
        String transactionRequest = "{\"amount\": 100 }";
        this.mockMvc.perform(post("/api/account/100/withdraw").with(auth).content(transactionRequest).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(404));
    }

    @Test
    void withdrawWithNonNumericIDReturns404() throws Exception {
        String transactionRequest = "{\"amount\": 100 }";
        this.mockMvc.perform(post("/api/account/test/withdraw").with(auth).content(transactionRequest).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(404));
    }

    /* DEPOSIT TESTS */

    @Test
    void depositWithoutAuthReturns401() throws Exception {
        this.mockMvc.perform(post("/api/account/1/deposit"))
                .andDo(print())
                .andExpect(status().is(401));
    }

    @Test
    void depositWithExistingIDAndInvalidAmountReturns400() throws Exception {
        String transactionRequest = "{\"amount\": -100 }";
        this.mockMvc.perform(post("/api/account/1/deposit").with(auth).content(transactionRequest).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(400));
    }

    @Test
    void depositWithExistingIDAndValidAmountReturnsNewBalanceAnd200() throws Exception {
        String transactionRequest = "{\"amount\": 100 }";
        this.mockMvc.perform(post("/api/account/1/deposit").with(auth).content(transactionRequest).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.balance").value(1100));
    }

    @Test
    void depositWithNotExistingIDReturns404() throws Exception {
        String transactionRequest = "{\"amount\": 100 }";
        this.mockMvc.perform(post("/api/account/100/deposit").with(auth).content(transactionRequest).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(404));
    }

    @Test
    void depositWithNonNumericIDReturns404() throws Exception {
        String transactionRequest = "{\"amount\": 100 }";
        this.mockMvc.perform(post("/api/account/test/deposit").with(auth).content(transactionRequest).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(404));
    }

    /* TRANSFER TESTS */

    @Test
    void transferWithoutAuthReturns401() throws Exception {
        this.mockMvc.perform(post("/api/account/transfer"))
                .andDo(print())
                .andExpect(status().is(401));
    }

    @Test
    void transferWithExistingIDsAndValidAmountReturns200AndUpdatesBalances() throws Exception {
        String transferRequest = "{\"sourceAccountId\": 1, \"destinationAccountId\": 2, \"amount\": 100 }";
        this.mockMvc.perform(post("/api/account/transfer").with(auth).content(transferRequest).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(200));

        this.mockMvc.perform(get("/api/account/1").with(auth))
                .andDo(print())
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.balance").value(900));

        this.mockMvc.perform(get("/api/account/2").with(auth))
                .andDo(print())
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.balance").value(300));
    }

    @Test
    void transferWithExistingIDsAndInvalidAmountReturns400() throws Exception {
        String transferRequest = "{\"sourceAccountId\": 1, \"destinationAccountId\": 2, \"amount\": -100 }";
        this.mockMvc.perform(post("/api/account/transfer").with(auth).content(transferRequest).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(400));
    }

    @Test
    void transferWithNotExistingSourceAccountIDReturns404() throws Exception {
        String transferRequest = "{\"sourceAccountId\": 1000, \"destinationAccountId\": 2, \"amount\": 100 }";
        this.mockMvc.perform(post("/api/account/transfer").with(auth).content(transferRequest).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(404));
    }

    @Test
    void transferWithNotExistingDestinationAccountIDReturns404() throws Exception {
        String transferRequest = "{\"sourceAccountId\": 1, \"destinationAccountId\": 2000, \"amount\": 100 }";
        this.mockMvc.perform(post("/api/account/transfer").with(auth).content(transferRequest).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(404));
    }

    /* LOAN TESTS */

    @Test
    void loanWithoutAuthReturns401() throws Exception {
        this.mockMvc.perform(post("/api/account/1/credit/loan"))
                .andDo(print())
                .andExpect(status().is(401));
    }

    @Test
    void loanWithExistingIDReturnsAccountWith200IfBalanceIsMoreThanMinimumRequirement() throws Exception {
        String loan = "{\"loanAmount\": 1000, \"interestRate\": 0.07}";
        this.mockMvc.perform(post("/api/account/1/credit/loan").with(auth).content(loan).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.creditFacilities.*.loanAmount").value(1000.0));
    }

    @Test
    void loanWithExistingIDReturns400IfBalanceIsLessThanMinimumRequirement() throws Exception {
        String loan = "{\"loanAmount\": 1000, \"interestRate\": 0.07}";
        this.mockMvc.perform(post("/api/account/2/credit/loan").with(auth).content(loan).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(400));
    }

    @Test
    void loanWithNotExistingIDReturns404() throws Exception {
        String loan = "{\"loanAmount\": 1000, \"interestRate\": 0.07}";
        this.mockMvc.perform(post("/api/account/100/credit/loan").with(auth).content(loan).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(404));
    }

    @Test
    void loanWithNonNumbericIDReturns404() throws Exception {
        String loan = "{\"loanAmount\": 1000, \"interestRate\": 0.07}";
        this.mockMvc.perform(post("/api/account/test/credit/loan").with(auth).content(loan).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(404));
    }
}
