package com.blockone.service;

import com.blockone.entity.Account;
import com.blockone.entity.User;
import com.blockone.error.AccoutNotFoundException;
import com.blockone.error.InvalidDepositException;
import com.blockone.error.InvalidWithdrawalException;
import com.blockone.error.NotEnoughBalanceException;
import com.blockone.repository.AccountRepository;
import com.blockone.service.account.SimpleAccountService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class SimpleAccountServiceTest {
    @Autowired
    SimpleAccountService accountService;

    @MockBean
    AccountRepository repository;

    long existingId = 1L;
    long notExistingId = 100L;
    long anotherExistingId = 2L;

    @Test
    void getAccountIfItExists() throws AccoutNotFoundException {
        accountService.getAccount(existingId);

        verify(repository).getAccount(existingId);
    }

    @Test
    void getAccountFailsIfAccountDoesNotExist() throws AccoutNotFoundException {
        Mockito.doThrow(new AccoutNotFoundException("")).when(repository).getAccount(notExistingId);

        assertThrows(AccoutNotFoundException.class, () -> accountService.getAccount(notExistingId));
    }

    @Test
    void insertAccount() {
        Account account = new Account(new User("someOwner"), 200.0);
        accountService.createAccount(account);

        verify(repository).insertAccount(account);
    }

    @Test
    void deleteAccountIfItExists() throws AccoutNotFoundException {
        accountService.deleteAccount(existingId);

        verify(repository).deleteAccount(existingId);
    }

    @Test
    void deleteAccountFailsIfAccountDoesNotExist() throws AccoutNotFoundException {
        Mockito.doThrow(new AccoutNotFoundException("")).when(repository).deleteAccount(notExistingId);

        assertThrows(AccoutNotFoundException.class, () -> accountService.deleteAccount(notExistingId));
    }

    @Test
    void updateAccountIfItExists() throws AccoutNotFoundException {
        Account account = new Account(new User("newOwner"), 300);
        Account oldAccount = new Account(existingId, new User("oldOwner"), 600);
        Mockito.doReturn(oldAccount).when(repository).getAccount(existingId);

        accountService.updateAccount(existingId, account);
        verify(repository).updateAccount(existingId, account);
    }

    @Test
    void updateAccountFailsIfAccountDoesNotExist() throws AccoutNotFoundException {
        Account account = new Account(null);
        Mockito.doThrow(new AccoutNotFoundException("")).when(repository).getAccount(notExistingId);

        assertThrows(AccoutNotFoundException.class, () -> accountService.updateAccount(notExistingId, account));
    }

    @Test
    void withdrawFailsIfNotEnoughBalance() throws AccoutNotFoundException {
        Account acc = new Account(new User("owner"), 100);
        Mockito.doReturn(acc).when(repository).getAccount(existingId);

        assertThrows(NotEnoughBalanceException.class, () -> accountService.withdraw(existingId, 200));
    }

    @Test
    void withdrawFailsIfWithdrawalAmountIsNegative() throws InvalidWithdrawalException, AccoutNotFoundException {
        Account acc = new Account(new User("owner"), 100);
        Mockito.doReturn(acc).when(repository).getAccount(existingId);

        assertThrows(InvalidWithdrawalException.class, () -> accountService.withdraw(existingId, -50));
    }

    @Test
    void withdrawUpdatesBalance() throws InvalidWithdrawalException, AccoutNotFoundException {
        Account acc = new Account(new User("owner"), 100);
        Mockito.doReturn(acc).when(repository).getAccount(existingId);

        accountService.withdraw(existingId, 100);
        assertEquals(0, acc.getBalance());
        verify(repository).updateAccount(existingId, acc);
    }

    @Test
    void depositFailsIfDepositAmountIsNegative() throws InvalidDepositException, AccoutNotFoundException {
        Account acc = new Account(new User("owner"), 100);
        Mockito.doReturn(acc).when(repository).getAccount(existingId);

        assertThrows(InvalidDepositException.class, () -> accountService.deposit(existingId, -50));
    }

    @Test
    void depositUpdatesBalanceIfDepositAmountIsPositive() throws AccoutNotFoundException, InvalidDepositException {
        Account acc = new Account(new User("owner"), 100);
        Mockito.doReturn(acc).when(repository).getAccount(existingId);

        accountService.deposit(existingId, 60);
        assertEquals(160, acc.getBalance());
        verify(repository).updateAccount(existingId, acc);
    }

    @Test
    void transferFailsIfSourceAccountDoesntExist() throws AccoutNotFoundException {
        Mockito.doThrow(new AccoutNotFoundException("")).when(repository).getAccount(notExistingId);

        assertThrows(AccoutNotFoundException.class, () -> accountService.transfer(notExistingId, existingId, 60));
    }

    @Test
    void transferFailsIfDestinationAccountDoesntExist() throws AccoutNotFoundException {
        Account acc = new Account(new User("owner"), 100);
        Mockito.doReturn(acc).when(repository).getAccount(existingId);
        Mockito.doThrow(new AccoutNotFoundException("")).when(repository).getAccount(notExistingId);

        assertThrows(AccoutNotFoundException.class, () -> accountService.transfer(existingId, notExistingId, 60));
        assertEquals(100, acc.getBalance());
    }

    @Test
    void transferFailsIfAmountIsInvalid() throws AccoutNotFoundException {
        Account acc = new Account(new User("owner"), 100);
        Account anotherAcc = new Account(new User("owner2"), 150);
        Mockito.doReturn(acc).when(repository).getAccount(existingId);

        assertThrows(InvalidWithdrawalException.class, () -> accountService.transfer(existingId, anotherExistingId, -50));
        assertEquals(100, acc.getBalance());
        assertEquals(150, anotherAcc.getBalance());
    }

    @Test
    void transferWorksIfAllConditionsMet() throws AccoutNotFoundException, InvalidWithdrawalException, InvalidDepositException {
        Account acc = new Account(new User("owner"), 100);
        Account anotherAcc = new Account(new User("owner2"), 150);
        Mockito.doReturn(acc).when(repository).getAccount(existingId);
        Mockito.doReturn(anotherAcc).when(repository).getAccount(anotherExistingId);

        accountService.transfer(existingId, anotherExistingId, 50);
        assertEquals(50, acc.getBalance());
        assertEquals(200, anotherAcc.getBalance());
    }
}
