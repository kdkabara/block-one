package com.blockone.service;

import com.blockone.entity.Account;
import com.blockone.entity.User;
import com.blockone.entity.creditfacility.CreditFacility;
import com.blockone.error.creditfacility.CreditFacilityException;
import com.blockone.service.creditfacility.CreditFacilityService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.mockito.Mockito.verify;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class CreditFacilityServiceTest {
    @Autowired
    CreditFacilityService creditFacilityService;

    @Mock
    CreditFacility creditFacility;

    @Test
    void creditFacilityServiceCanAddCreditFacility() throws CreditFacilityException {
        Account account = new Account(new User("TestUser"));
        creditFacilityService.addCreditFacility(account, creditFacility);

        verify(creditFacility).addToAccount(account);
    }
}
