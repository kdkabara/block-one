package com.blockone.repository;

import com.blockone.entity.Account;
import com.blockone.entity.User;
import com.blockone.error.AccoutNotFoundException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class InMemoryAccountRepositoryTest {
    InMemoryAccountRepository accountRepository;

    @BeforeEach
    void setUp() {
        Map<Long, Account> accounts = new HashMap<>();
        accounts.put(1L, new Account(new User("owner"), 100));
        accounts.put(2L, new Account(new User("owner2"), 200));
        accountRepository = new InMemoryAccountRepository(accounts);
    }

    @AfterEach
    void tearDown() {
        accountRepository = null;
    }

    @Test
    void getAccountIfItExists() throws AccoutNotFoundException {
        Account acc = accountRepository.getAccount(1L);
        assertEquals("owner", acc.getOwner().getName());
        assertEquals(100, acc.getBalance());
    }

    @Test
    void getAccountFailsIfAccountDoesNotExist() {
        assertThrows(AccoutNotFoundException.class, () -> accountRepository.getAccount(3L));
    }

    @Test
    void insertAccount() throws AccoutNotFoundException {
        User owner = new User("owner3");
        double balance = 300;
        Account inserted = accountRepository.insertAccount(new Account(owner, balance));

        Account acc = accountRepository.getAccount(inserted.getId());
        assertEquals(owner, acc.getOwner());
        assertEquals(balance, acc.getBalance());
    }

    @Test
    void updateAccountIfItExists() throws AccoutNotFoundException {
        long accountId = 1L;
        User owner = new User("newOwner");
        int balance = 300;
        Account acc = accountRepository.updateAccount(accountId, new Account(owner, balance));

        assertEquals(owner, acc.getOwner());
        assertEquals(balance, acc.getBalance());
    }

    @Test
    void updateAccountFailsIfAccountDoesNotExist() {
        assertThrows(AccoutNotFoundException.class, () -> accountRepository.getAccount(3L));
    }

    @Test
    void deleteAccountIfItExists() throws AccoutNotFoundException {
        long accountId = 1L;
        accountRepository.deleteAccount(accountId);

        assertThrows(AccoutNotFoundException.class, () -> accountRepository.getAccount(accountId));
    }

    @Test
    void deleteAccountFailsIfAccountDoesNotExist() {
        assertThrows(AccoutNotFoundException.class, () -> accountRepository.getAccount(3L));
    }
}
