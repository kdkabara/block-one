package com.blockone.service.creditfacility;

import com.blockone.entity.Account;
import com.blockone.entity.creditfacility.CreditFacility;
import com.blockone.error.creditfacility.CreditFacilityException;
import org.springframework.stereotype.Component;

@Component("CreditFacilityProvider")
public class CreditFacilityService implements CreditFacilityProvider {
    @Override
    public Account addCreditFacility(Account account, CreditFacility creditFacility) throws CreditFacilityException {
        creditFacility.addToAccount(account);
        return account;
    }
}
