package com.blockone.service.creditfacility;

import com.blockone.entity.Account;
import com.blockone.entity.creditfacility.CreditFacility;
import com.blockone.error.creditfacility.CreditFacilityException;

public interface CreditFacilityProvider {
    public Account addCreditFacility(Account account, CreditFacility creditFacility) throws CreditFacilityException;
}
