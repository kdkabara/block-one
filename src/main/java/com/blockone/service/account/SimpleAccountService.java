package com.blockone.service.account;

import com.blockone.entity.Account;
import com.blockone.error.AccoutNotFoundException;
import com.blockone.error.InvalidDepositException;
import com.blockone.error.InvalidWithdrawalException;
import com.blockone.error.NotEnoughBalanceException;
import com.blockone.repository.AccountRepository;
import org.springframework.stereotype.Component;

@Component("AccountService")
public class SimpleAccountService implements AccountService {
    private AccountRepository accountRepository;

    public SimpleAccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public Account getAccount(long id) throws AccoutNotFoundException {
        return accountRepository.getAccount(id);
    }

    @Override
    public Account createAccount(Account account) {
        return accountRepository.insertAccount(account);
    }

    @Override
    public boolean deleteAccount(long id) throws AccoutNotFoundException {
        return accountRepository.deleteAccount(id);
    }

    @Override
    public Account updateAccount(long id, Account account) throws AccoutNotFoundException {
        Account currentAccount = getAccount(id);
        account.setCurrency(currentAccount.getCurrency());
        account.setBalance(currentAccount.getBalance());
        account.setCreditFacilities(currentAccount.getCreditFacilities());
        return accountRepository.updateAccount(id, account);
    }

    @Override
    public Account withdraw(long id, double amount) throws AccoutNotFoundException, InvalidWithdrawalException {
        Account acc = this.getAccount(id);
        if (amount < 0) {
            throw new InvalidWithdrawalException("Withdrawal amount cannot be negative");
        }

        if (amount > acc.getBalance()) {
            throw new NotEnoughBalanceException(String.format("Not enough balance in account [%s], current balance: [%s]", id, acc.getBalance()));
        } else {
            System.out.println(String.format("Withdrawing [%s] from [%s]", amount, id));
            double newBalance = acc.getBalance() - amount;
            acc.setBalance(newBalance);
            return this.updateAccount(id, acc);
        }
    }

    @Override
    public Account deposit(long id, double amount) throws AccoutNotFoundException, InvalidDepositException {
        Account acc = this.getAccount(id);
        if (amount < 0) {
            throw new InvalidDepositException("Deposit amount cannot be negative");
        }

        System.out.println(String.format("Depositing [%s] into [%s]", amount, id));
        double newBalance = acc.getBalance() + amount;
        acc.setBalance(newBalance);
        return this.updateAccount(id, acc);
    }

    @Override
    public void transfer(long source, long destination, double amount) throws InvalidWithdrawalException, AccoutNotFoundException, InvalidDepositException {
        try {
            getAccount(source);
            getAccount(destination);
        } catch (AccoutNotFoundException e) {
            throw e;
        }

        System.out.println(String.format("Transferring [%s] from [%s] to [%s]", amount, source, destination));
        withdraw(source, amount);
        deposit(destination, amount);
    }
}
