package com.blockone.service.account;

import com.blockone.entity.Account;
import com.blockone.error.AccoutNotFoundException;
import com.blockone.error.InvalidDepositException;
import com.blockone.error.InvalidWithdrawalException;

public interface AccountService {
    Account getAccount(long id) throws AccoutNotFoundException;

    Account createAccount(Account account);

    boolean deleteAccount(long id) throws AccoutNotFoundException;

    Account updateAccount(long id, Account account) throws AccoutNotFoundException;

    Account withdraw(long id, double amount) throws AccoutNotFoundException, InvalidWithdrawalException;

    Account deposit(long id, double amount) throws AccoutNotFoundException, InvalidDepositException;

    void transfer(long source, long destination, double amount) throws AccoutNotFoundException, InvalidDepositException, InvalidWithdrawalException;
}
