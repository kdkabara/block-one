package com.blockone.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class InvalidDepositException extends Exception {
    public InvalidDepositException(String message) {
        super(message);
    }
}
