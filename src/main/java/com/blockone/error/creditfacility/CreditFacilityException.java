package com.blockone.error.creditfacility;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class CreditFacilityException extends Exception {
    public CreditFacilityException(String message) {
        super(message);
    }
}
