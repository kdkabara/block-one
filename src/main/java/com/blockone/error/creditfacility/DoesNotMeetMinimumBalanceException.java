package com.blockone.error.creditfacility;

public class DoesNotMeetMinimumBalanceException extends CreditFacilityException {
    public DoesNotMeetMinimumBalanceException(String message) {
        super(message);
    }
}
