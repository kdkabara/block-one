package com.blockone.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class NotEnoughBalanceException extends InvalidWithdrawalException {
    public NotEnoughBalanceException(String message) {
        super(message);
    }
}
