package com.blockone.entity.creditfacility;

import com.blockone.entity.Account;
import com.blockone.error.creditfacility.DoesNotMeetMinimumBalanceException;

public class SimpleLoan extends CreditFacility {

    double loanAmount;
    double interestRate;
    final static double MINIMUM_BALANCE_REQUIREMENT = 1000;

    public SimpleLoan() {
    }

    public SimpleLoan(double loanAmount, double interestRate) {
        this.loanAmount = loanAmount;
        this.interestRate = interestRate;
    }

    public double getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(double loanAmount) {
        this.loanAmount = loanAmount;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }

    @Override
    public boolean addToAccount(Account account) throws DoesNotMeetMinimumBalanceException {
        double currentBalance = account.getBalance();
        if (currentBalance < MINIMUM_BALANCE_REQUIREMENT) {
            throw new DoesNotMeetMinimumBalanceException(String.format("Account [%s] does not meet the minimum balance requirement for this loan, requirement: [%s], current balance: [%s]", account.getId(), MINIMUM_BALANCE_REQUIREMENT, currentBalance));
        }
        account.insertCreditFacility(this);
        return true;
    }
}
