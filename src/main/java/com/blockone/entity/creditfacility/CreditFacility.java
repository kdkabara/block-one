package com.blockone.entity.creditfacility;

import com.blockone.entity.Account;
import com.blockone.error.creditfacility.CreditFacilityException;

public abstract class CreditFacility {
    private static long FACILITY_COUNTER = 0;

    long id;

    public CreditFacility() {
        this(++FACILITY_COUNTER);
    }

    public CreditFacility(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public abstract boolean addToAccount(Account account) throws CreditFacilityException;
}
