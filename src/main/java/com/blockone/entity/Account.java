package com.blockone.entity;

import com.blockone.entity.creditfacility.CreditFacility;

import java.util.HashMap;
import java.util.Map;

public class Account {
    private static long ACCOUNT_COUNTER = 0;

    private final long id;

    private final User owner;
    private String currency = "HKD";

    private double balance;

    private Map<Long, CreditFacility> creditFacilities = new HashMap<>();

    public Account() {
        this.id = ++ACCOUNT_COUNTER;
        this.owner = null;
    }

    public Account(long id, User owner, double balance) {
        this.id = id;
        this.owner = owner;
        this.balance = balance;
    }

    public Account(User owner, double balance) {
        this(++ACCOUNT_COUNTER, owner, balance);
    }

    public Account(User owner) {
        this(owner, 0);
    }

    public long getId() {
        return id;
    }

    public User getOwner() {
        return owner;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Map<Long, CreditFacility> getCreditFacilities() {
        return creditFacilities;
    }

    public void setCreditFacilities(Map<Long, CreditFacility> creditFacilities) {
        this.creditFacilities = creditFacilities;
    }

    public void insertCreditFacility(CreditFacility creditFacility) {
        this.creditFacilities.put(creditFacility.getId(), creditFacility);
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", owner='" + owner + '\'' +
                ", balance=" + balance +
                '}';
    }
}
