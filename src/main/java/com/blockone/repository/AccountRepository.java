package com.blockone.repository;

import com.blockone.entity.Account;
import com.blockone.error.AccoutNotFoundException;

public interface AccountRepository {
    public Account getAccount(long accountId) throws AccoutNotFoundException;

    public Account insertAccount(Account account);

    public Account updateAccount(long accountId, Account account) throws AccoutNotFoundException;

    public boolean deleteAccount(long accountId) throws AccoutNotFoundException;
}
