package com.blockone.repository;

import com.blockone.entity.Account;
import com.blockone.error.AccoutNotFoundException;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component("AccountRepository")
public class InMemoryAccountRepository implements AccountRepository {
    private Map<Long, Account> accounts;

    public InMemoryAccountRepository() {
        this.accounts = new HashMap<>();
    }

    public InMemoryAccountRepository(Map<Long, Account> accounts) {
        this.accounts = accounts;
    }

    @Override
    public Account getAccount(long accountId) throws AccoutNotFoundException {
        if (accounts.containsKey(accountId)) {
            System.out.println(String.format("Getting account with account ID: [%s]", accountId));
            return accounts.get(accountId);
        } else {
            throw new AccoutNotFoundException(String.format("Could not find account with account ID: [%s]", accountId));
        }
    }

    @Override
    public Account insertAccount(Account account) {
        System.out.println(String.format("Inserting new account: [%s]", account));
        accounts.put(account.getId(), account);
        return accounts.get(account.getId());
    }

    @Override
    public Account updateAccount(long accountId, Account account) throws AccoutNotFoundException {
        if (accounts.containsKey(accountId)) {
            System.out.println(String.format("Updating account [%s]: [%s]", accountId, account));
            accounts.put(accountId, account);
            return accounts.get(accountId);
        } else {
            throw new AccoutNotFoundException(String.format("Could not find account with account ID: [%s]", accountId));
        }
    }

    @Override
    public boolean deleteAccount(long accountId) throws AccoutNotFoundException {
        if (accounts.containsKey(accountId)) {
            System.out.println(String.format("Deleting account [%s]", accountId));
            accounts.remove(accountId);
            return true;
        } else {
            throw new AccoutNotFoundException(String.format("Could not find account with account ID: [%s]", accountId));
        }
    }
}
