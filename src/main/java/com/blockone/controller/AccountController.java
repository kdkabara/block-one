package com.blockone.controller;

import com.blockone.entity.Account;
import com.blockone.entity.creditfacility.SimpleLoan;
import com.blockone.entity.request.TransactionRequest;
import com.blockone.entity.request.TransferRequest;
import com.blockone.error.AccoutNotFoundException;
import com.blockone.error.InvalidDepositException;
import com.blockone.error.InvalidWithdrawalException;
import com.blockone.error.creditfacility.CreditFacilityException;
import com.blockone.error.request.InvalidRequestException;
import com.blockone.service.account.AccountService;
import com.blockone.service.creditfacility.CreditFacilityProvider;
import org.springframework.web.bind.annotation.*;

@RestController
public class AccountController {
    private AccountService accountService;
    private CreditFacilityProvider creditFacilityProvider;

    public AccountController(AccountService accountService, CreditFacilityProvider creditFacilityProvider) {
        this.accountService = accountService;
        this.creditFacilityProvider = creditFacilityProvider;
    }

    @GetMapping("/api/account/{id}")
    public Account getAccount(@PathVariable String id) throws AccoutNotFoundException {
        try {
            return accountService.getAccount(Long.valueOf(id));
        } catch (NumberFormatException e) {
            throw new AccoutNotFoundException(String.format("Could not find account with account ID: [%s]", id));
        }
    }

    @PostMapping("/api/account")
    public Account insertAccount(@RequestBody Account account) {
        return accountService.createAccount(account);
    }

    @PutMapping("/api/account/{id}")
    public Account updateAccount(@PathVariable String id, @RequestBody Account account) throws InvalidRequestException, AccoutNotFoundException {
        long parsedId;
        try {
            parsedId = Long.valueOf(id);
        } catch (NumberFormatException e) {
            throw new AccoutNotFoundException(String.format("Could not find account with account ID: [%s]", id));
        }

        if (parsedId != account.getId()) throw new InvalidRequestException("IDs don't match");

        return accountService.updateAccount(parsedId, account);
    }

    @DeleteMapping("/api/account/{id}")
    public void deleteAccount(@PathVariable String id) throws AccoutNotFoundException {
        try {
            accountService.deleteAccount(Long.valueOf(id));
        } catch (NumberFormatException e) {
            throw new AccoutNotFoundException(String.format("Could not find account with account ID: [%s]", id));
        }
    }

    @PostMapping("/api/account/{id}/withdraw")
    public Account withdrawFromAccount(@PathVariable String id, @RequestBody TransactionRequest transaction) throws AccoutNotFoundException, InvalidWithdrawalException {
        try {
            return accountService.withdraw(Long.valueOf(id), transaction.getAmount());
        } catch (NumberFormatException e) {
            throw new AccoutNotFoundException(String.format("Could not find account with account ID: [%s]", id));
        }
    }

    @PostMapping("/api/account/{id}/deposit")
    public Account depositIntoAccount(@PathVariable String id, @RequestBody TransactionRequest transaction) throws AccoutNotFoundException, InvalidDepositException {
        try {
            return accountService.deposit(Long.valueOf(id), transaction.getAmount());
        } catch (NumberFormatException e) {
            throw new AccoutNotFoundException(String.format("Could not find account with account ID: [%s]", id));
        }
    }

    @PostMapping("/api/account/transfer")
    public void transferRequest(@RequestBody TransferRequest transferRequest) throws InvalidDepositException, AccoutNotFoundException, InvalidWithdrawalException {
        accountService.transfer(transferRequest.getSourceAccountId(), transferRequest.getDestinationAccountId(), transferRequest.getAmount());
    }

    @PostMapping("/api/account/{id}/credit/loan")
    public Account addLoan(@PathVariable String id, @RequestBody SimpleLoan loan) throws AccoutNotFoundException, CreditFacilityException {
        try {
            Account account = accountService.getAccount(Long.valueOf(id));
            return creditFacilityProvider.addCreditFacility(account, loan);
        } catch (NumberFormatException e) {
            throw new AccoutNotFoundException(String.format("Could not find account with account ID: [%s]", id));
        }
    }
}
